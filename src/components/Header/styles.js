import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  justify-content: center;
  margin-top: 50px;
  margin-bottom: 50px;

  .logo {
    width: 200px;
  }
`;
