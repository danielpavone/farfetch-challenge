import React from 'react';
import PropTypes from 'prop-types';

import { List } from './styles';

export default function User({ users, checkOutUser, editUser }) {
  return (
    <div className="columns is-multiline">
      {users.map(user => (
        <div key={user.id} className="column is-4">
          <div className="card">
            <div className="card-content">
              <div>
                <List>
                  <li>
                    <b>Name: </b>
                    {user.firstName
                      ? `${user.firstName} ${user.lastName}`
                      : '-'}
                  </li>
                  <li>
                    <b>Email: </b>
                    {user.email ? user.email : '-'}
                  </li>
                  <li>
                    <b>Postal Code: </b>
                    {user.postalCode ? user.postalCode : '-'}
                  </li>
                  <li>
                    <b>City: </b>
                    {user.city ? user.city : '-'}
                  </li>
                  <li>
                    <b>Country: </b>
                    {user.country ? user.country : '-'}
                  </li>
                </List>
              </div>
            </div>
            <footer className="card-footer">
              <button
                onClick={() => editUser(user)}
                type="button"
                className="button is-fullwidth is-primary"
              >
                Edit
              </button>
              <button
                onClick={() => checkOutUser(user.id)}
                type="button"
                className="button is-fullwidth is-danger"
              >
                Checkout
              </button>
            </footer>
          </div>
        </div>
      ))}
    </div>
  );
}

User.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      user: PropTypes.shape({
        id: PropTypes.string,
        email: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        postalCode: PropTypes.string,
        city: PropTypes.string,
        country: PropTypes.string,
      }),
    })
  ).isRequired,
  checkOutUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
};
