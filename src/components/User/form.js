import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

const EditUserForm = ({ currentUser, updateUser, setEditing }) => {
  const [user, setUser] = useState(currentUser);

  const handleInputChange = event => {
    const { name, value } = event.target;

    setUser({ ...user, [name]: value });
  };

  useEffect(() => {
    setUser(currentUser);
  }, [currentUser]);

  return (
    <div className="columns">
      <form
        onSubmit={event => {
          event.preventDefault();
          updateUser(user.id, user);
        }}
      >
        <label>
          First Name
          <input
            type="text"
            name="firstName"
            value={user.firstName}
            onChange={handleInputChange}
            className="input"
          />
        </label>
        <label>
          Last Name
          <input
            type="text"
            name="lastName"
            value={user.lastName}
            onChange={handleInputChange}
            className="input"
          />
        </label>
        <label>Email:</label>
        <input
          type="email"
          name="email"
          value={user.email}
          onChange={handleInputChange}
          className="input"
        />
        <label>
          Postal Code
          <input
            type="text"
            name="postalCode"
            value={user.postalCode}
            onChange={handleInputChange}
            className="input"
          />
        </label>
        <label>
          City
          <input
            type="text"
            name="city"
            value={user.city}
            onChange={handleInputChange}
            className="input"
          />
        </label>
        <label>Country</label>
        <input
          type="text"
          name="country"
          value={user.country}
          onChange={handleInputChange}
          className="input"
        />
        <button type="button" className="button">
          Update
        </button>
        <button
          type="button"
          onClick={() => setEditing(false)}
          className="button muted-button"
        >
          Cancel
        </button>
      </form>
    </div>
  );
};

EditUserForm.propTypes = {
  currentUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  setEditing: PropTypes.func.isRequired,
};

export default EditUserForm;
