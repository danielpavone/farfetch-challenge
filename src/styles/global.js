import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background-color: #ffffff;
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font-size: 14px;
  }

  ul {
    list-style-type: none !important;
  }

  #root {
    max-width: 1170px;
    margin: 0 auto;
    padding: 0 20px 50px;
  }

  button {
    cursor: pointer;
  }
`;
