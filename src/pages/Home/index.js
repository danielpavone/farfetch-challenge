import React, { useEffect, useState, useCallback } from 'react';
import { w3cwebsocket as W3CWebSocket } from 'websocket';

import api from '../../services/api';

import User from '../../components/User';
import EditUserForm from '../../components/User/form';

const client = new W3CWebSocket('ws://0.0.0.0:7000/ws');

export default function Home() {
  const initialFormState = {
    id: '',
    firstName: '',
    lastName: '',
    email: '',
    city: '',
    postalCode: '',
    country: '',
  };

  const [users, setUsers] = useState([]);
  const [editing, setEditing] = useState(false);
  const [error, setError] = useState(false);
  const [currentUser, setCurrentUser] = useState(initialFormState);

  // get users from websocket and update the users
  const checkInUserWebsocket = useCallback(() => {
    client.onmessage = message => {
      const user = JSON.parse(message.data);
      setUsers([...users, user.user]);
    };
  }, [users]);

  useEffect(() => {
    checkInUserWebsocket();
  }, [checkInUserWebsocket]);

  useEffect(() => {
    // load user that checked in api
    const loadUsers = async () => {
      const response = await api.get('api/users');
      setUsers(response.data.users);
    };

    loadUsers();
  }, []);

  // checkout user from store
  const checkOutUser = async id => {
    try {
      await api.put(`api/users/${id}/checkout`);

      const checkInUsers = users.filter(user => user.id !== id);
      setUsers(checkInUsers);
      setEditing(false);
    } catch (err) {
      setError(true);
    }
  };

  // enable edit mode
  const editUser = async user => {
    setEditing(true);
    setCurrentUser(user);
  };

  // update user
  const updateUser = async (id, update) => {
    try {
      const payload = {
        user: { ...update },
      };

      await api.put(`api/users/${id}`, payload);
      setEditing(false);
      setUsers(users.map(user => (user.id === id ? update : user)));
    } catch (err) {
      setEditing(true);
      setError(true);
    }
  };

  return (
    <>
      {editing ? (
        <EditUserForm
          setEditing={setEditing}
          currentUser={currentUser}
          updateUser={updateUser}
        />
      ) : (
        ''
      )}
      <h4 className="has-text-centered is-size-4">Users on store</h4>
      <br />
      <User users={users} checkOutUser={checkOutUser} editUser={editUser} />
    </>
  );
}
